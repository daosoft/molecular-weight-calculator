﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Molecular_Weight_Calculator
{
    public class ElementDictionary
    {
        public static Dictionary<string, double> ElementDict()
        {
            Dictionary<string, double> elements = new Dictionary<string, double>();
            elements.Add("H", 1.008);
            elements.Add("C", 12.011);
            elements.Add("O", 15.999);
            elements.Add("N", 14.007);
            elements.Add("He", 4.003);
            elements.Add("Cu", 63.546);
            elements.Add("Na", 22.990);
            elements.Add("F", 18.998);
            elements.Add("Ca", 40.078);
            elements.Add("Mg", 24.305);
            return elements;
        }
    }
}

