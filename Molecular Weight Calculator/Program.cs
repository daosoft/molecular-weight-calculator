﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Molecular_Weight_Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Enter the sum formula of your molecule (e.g. C2H5OH or CaCO3):");
                Console.Write("Input: ");

                string enteredString = Console.ReadLine();
                string initialpattern = @"[A-Z][a-z]?\d*";

                List<string> splitElements = new List<string>(RegexMatch(enteredString, initialpattern)); //list of element segments (eg. C2, H5, O etc.)

                     
                string compareString = string.Join("", splitElements.ToArray());      //splitElements to string compare to enteredString, if a part of the inputstring wasnt handled by regex, then output an error
                if (enteredString.Equals(compareString) == true)
                {
                    List<double> totalSums = RegexSplit(splitElements, enteredString);        //list of the weights of each element

                    var totSum = totalSums.Sum();                                            //add all weights together
                    Console.WriteLine("\n_________");
                    Console.WriteLine($"{enteredString} = {totSum} g/mol\n\n\n");            //output total molecular weight.
                }
                else
                {
                    char[] compareArray = compareString.ToCharArray();
                    string errorString = enteredString.Trim(compareArray);
                    Console.WriteLine($"The input formula contains an error: {errorString}");
                }
            }
        }

            public static List<string> RegexMatch(string enteredString, string initialpattern) //Splits the initial input into an element and its count eg. C2, H5, O(1) etc.
            {
                List<string> regexlist = new List<string>();
                Match m = Regex.Match(enteredString, initialpattern);

                while (m.Success)
                {
                    string moleculePart = m.Value;
                    regexlist.Add(moleculePart);

                    m = m.NextMatch();
                }
                return regexlist;
            }

            public static List<double> RegexSplit(List<string> splitElements, string enteredString)
            {
                List<double> totalSums = new List<double>();             //saves the weight of each element multiplied by its count

                foreach (var substring in splitElements)                 //split each element segment into a string "elementName" and an int "elementCount"
                {
                    string patternElement = @"[A-Z][a-z]?";             //use pattern to get the element from string, eg. H or He
                    Match elementMatch = Regex.Match(substring, patternElement);

                    string elementName = elementMatch.Value;

                    string patternSum = @"\d+";                         //use pattern to get the elementCount from string, eg. the 2 in C2
                    Match sumMatch = Regex.Match(substring, patternSum);
                    string sumMatchString = sumMatch.Value;

                    int.TryParse(sumMatchString, out int elementCount);   //convert sumMatchString to int elementCount
                    if (elementCount == 0) elementCount = 1;              // if there isnt a number after an element, the elementCount is 1 



                    var elements = ElementDictionary.ElementDict();
                    if (elements.TryGetValue(elementName, out double elementWeight) == true)
                    {
                        double sum = elementWeight * elementCount;
                        totalSums.Add(sum);                                   //add the total weight of the element (weight*count) to list, totalSums.
                        Console.WriteLine($"{elementCount} {elementName}: {elementCount} x {elementWeight} = {sum} g/mol");
                    }
                    else
                    {
                        Console.Clear();                                            //if a combination of letters was handled by the pattern, but wasnt found in the dictionary (eg. A or Bn, output an error
                        var errorPosition = enteredString.IndexOf(elementName);
                        Console.WriteLine($"{elementName} at position {errorPosition + 1} is not an element, please enter a correct formula!");
                        break;
                    }
                }
                return totalSums;
            }

        }
    }

